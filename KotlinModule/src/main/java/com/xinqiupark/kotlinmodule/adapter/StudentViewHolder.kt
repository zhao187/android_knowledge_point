package com.qi

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.widget.TextView
import com.xinqiupark.kotlinmodule.R
import com.xinqiupark.kotlinmodule.db.Student

import android.view.ViewGroup

class StudentViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.student_item, parent, false)) {

    private val nameView = itemView.findViewById<TextView>(R.id.name)
    var student: Student? = null

    /**
     * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
     * ViewHolder when Item is loaded.
     */
    fun bindTo(student: Student?) {
        this.student = student
        nameView.text = student?.name
    }
}