package com.xinqiupark.kotlinmodule

import android.arch.lifecycle.AndroidViewModel
import android.app.Application
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.xinqiupark.kotlinmodule.db.Student
import com.xinqiupark.kotlinmodule.db.StudentDb

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/13
 * desc:ViewModel显示数据
 */
class MainViewModel(app: Application) : AndroidViewModel(app) {

    val dao = StudentDb.get(app).studentDao()

    val allStudents = LivePagedListBuilder(dao.getAllStudent(), PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setEnablePlaceholders(ENABLE_PLACEHOLDERS)
            .setInitialLoadSizeHint(PAGE_SIZE)
            .build()).build()

    fun insertStudent(name: String) {
        dao.insert(Student(0, name))
    }

    companion object {

        private const val PAGE_SIZE = 15

        private const val ENABLE_PLACEHOLDERS = false
    }
}