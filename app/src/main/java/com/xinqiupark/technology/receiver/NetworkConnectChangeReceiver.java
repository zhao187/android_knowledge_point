package com.xinqiupark.technology.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.xinqiupark.technology.event.NetWorkChangeEvent;
import com.xinqiupark.technology.util.NetUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/20
 * desc:网络 状态接收器
 */
public class NetworkConnectChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /*
        判断当前网络状态是否可用
         */
        boolean isConnected= NetUtils.isConnected(context);

        //发送当前网络状态
        EventBus.getDefault().post(new NetWorkChangeEvent(isConnected));
    }
}
