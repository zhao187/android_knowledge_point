package com.xinqiupark.technology.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/25
 * desc:通知接收 点击广播
 */
public class NotificationReceiver extends BroadcastReceiver {

    public static final String ACTION_2 = "PressNextButton";

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action =
                intent.getAction();

        if(ACTION_2.equals(action))
        {
            Toast.makeText(context, "下一曲按钮", Toast.LENGTH_SHORT).show();
        }
    }
}
