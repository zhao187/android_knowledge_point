package com.xinqiupark.technology.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.xinqiupark.technology.R;
import com.zhengsr.viewpagerlib.bean.PageBean;
import com.zhengsr.viewpagerlib.callback.PageHelperListener;
import com.zhengsr.viewpagerlib.indicator.ZoomIndicator;
import com.zhengsr.viewpagerlib.view.GlideViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GlideViewPagerActivity extends AppCompatActivity {

    @BindView(R.id.splase_viewpager)
    GlideViewPager mSplaseViewpager;
    @BindView(R.id.splase_start_btn)
    Button mSplaseStartBtn;
    @BindView(R.id.splase_bottom_layout)
    ZoomIndicator mSplaseBottomLayout;

    private int mImages[]={R.drawable.icon_tu1,R.drawable.icon_tu2,
            R.drawable.icon_tu3,R.drawable.icon_tu4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide_view_pager);
        ButterKnife.bind(this);

        initView();
    }

    /*
    初始化视图
     */
    private void initView() {
        //1.先把本地的图片 id 装进 list 容器中
        List<Integer> images=new ArrayList<>();
        for (int mImage : mImages) {
            images.add(mImage);
        }

        //配置pagerbean，这里主要是为了viewpager的指示器的作用，注意记得写上泛型
        PageBean bean = new PageBean.Builder<Integer>()
                .setDataObjects(images)
                .setIndicator(mSplaseBottomLayout)
                .setOpenView(mSplaseStartBtn)
                .builder();

        // 把数据添加到 viewpager中，并把view提供出来，这样除了方便调试，也不会出现一个view，多个
        // parent的问题，这里在轮播图比较明显
        mSplaseViewpager.setPageListener(bean, R.layout.image_layout, (view, o) -> {
            ImageView imageView=view.findViewById(R.id.iv_icon);
            imageView.setImageResource((Integer)o);
        });
    }
}
