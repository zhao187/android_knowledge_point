package com.xinqiupark.technology.ui.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xinqiupark.technology.R;

import java.util.List;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/31
 * desc:RecycerlView列表适配
 */
public class ListAdapter extends BaseQuickAdapter<String,BaseViewHolder>{

    public ListAdapter(int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder holder, String item) {
        holder.setText(R.id.tv_title,item);
    }
}
