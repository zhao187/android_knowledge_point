package com.xinqiupark.technology.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.StackView;
import android.widget.TextView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.ImageAdapter;
import com.xinqiupark.technology.ui.adapter.StackImageAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StackViewActivity extends AppCompatActivity {

    @BindView(R.id.textview)
    TextView mTextview;
    @BindView(R.id.btn_down)
    Button mBtnDown;
    @BindView(R.id.btn_up)
    Button mBtnUp;
    @BindView(R.id.stackview)
    StackView mStackview;


    private ArrayList<Integer> mImages=new ArrayList<Integer>(){{
        add(R.drawable.icon_tu1);
        add(R.drawable.icon_tu2);
        add(R.drawable.icon_tu3);
        add(R.drawable.icon_tu4);
    }};

    private StackImageAdapter mStackImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack_view);
        ButterKnife.bind(this);

        mStackImageAdapter = new StackImageAdapter(mImages, this);
        mStackview.setAdapter(mStackImageAdapter);
        mStackview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mTextview.setText("第"+(position+1)+"个图片");
            }
        });

        mBtnDown.setOnClickListener(view -> {
            mStackview.showNext();
        });

        mBtnUp.setOnClickListener(view->{
            mStackview.showPrevious();
        });
    }
}
