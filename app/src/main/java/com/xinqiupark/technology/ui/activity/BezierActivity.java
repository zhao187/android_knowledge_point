package com.xinqiupark.technology.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.xinqiupark.technology.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BezierActivity extends AppCompatActivity {

    @BindView(R.id.btn_wave)
    Button mBtnWave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bezier);
        ButterKnife.bind(this);

        mBtnWave.setOnClickListener(view->{
            startActivity(new Intent(this,WaveActivity.class));
        });
    }
}
