package com.xinqiupark.technology.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.xinqiupark.technology.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SaripaarActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty(trim = true,message = "用户名不能为空")
    @Order(1)
    @BindView(R.id.et_username)
    EditText mEtUserName;

    @NotEmpty(trim = true,message = "输入密码不能为空")
    @Password(min = 6,message = "密码为数字和字母",scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS)
    @Order(2)
    @BindView(R.id.et_pwd)
    EditText mEtPwd;

    @OnClick(R.id.btn_login)
    public void onLoginClick()
    {
        mValidator.validate();
    }

    private Validator mValidator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saripaar);
        ButterKnife.bind(this);

        mValidator=new Validator(this);
        //按照顺序第一个不符合规则的
        mValidator.setValidationMode(Validator.Mode.IMMEDIATE);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error:errors)
        {
            View view=error.getView();
            String message=error.getCollatedErrorMessage(this);

            //显示错误信息
            if(view instanceof EditText)
            {
                ((EditText) view).setError(message);
            }else
            {
                Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
            }
        }
    }
}
