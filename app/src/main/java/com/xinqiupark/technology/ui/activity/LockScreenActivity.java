package com.xinqiupark.technology.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.service.LockScreenService;

public class LockScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);

        findViewById(R.id.start_service_btn).setOnClickListener(view -> {
            Intent intent = new Intent(LockScreenActivity.this,
                    LockScreenService.class);
            startService(intent); //启动后台服务
        });
    }
}
