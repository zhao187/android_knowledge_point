package com.xinqiupark.technology;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.xinqiupark.technology.event.NetWorkChangeEvent;
import com.xinqiupark.technology.util.NetUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/20
 * desc:全局 Activity
 */
public class BaseActivity extends AppCompatActivity {

    private Context mContext;

    private boolean mCheckNetWork = true;//默认检查网络

    View mTipView;

    WindowManager mWindowManager;

    WindowManager.LayoutParams mLayoutParams;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        initTipView();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //在无网络情况下打开app时，系统不会发送网络状态变更的intent,需要自己手动检查
        hasNetWork(NetUtils.isConnected(mContext));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void finish() {
        super.finish();

        //当提示View被动态添加后直接关闭导致该View内存溢出，所以需要在finish移除
        if (mTipView != null && mTipView.getParent() != null) {
            mWindowManager.removeView(mTipView);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetWorkChangeEvent(NetWorkChangeEvent event) {
        hasNetWork(event.isConnected);
    }

    /*
    是否有网络链接
     */
    private void hasNetWork(boolean isConnected) {
        if (isCheckNetWork()) {
            if (isConnected) {
                if (mTipView != null && mTipView.getParent()!=null)
                {
                    mWindowManager.removeView(mTipView);
                }
            }
            else {
                if(mTipView.getParent()==null)
                {
                    mWindowManager.addView(mTipView,mLayoutParams);
                }
            }
        }
    }

    /*
    设置网络
     */
    public void setCheckNetWork(boolean checkNetWork)
    {
        this.mCheckNetWork=checkNetWork;
    }

    /*
    检测网络状态
     */
    public boolean isCheckNetWork() {
        return mCheckNetWork;
    }


    /*
                初始化提示窗口
                 */
    private void initTipView() {
        LayoutInflater inflater = getLayoutInflater();

        mTipView = inflater.inflate(R.layout.layout_network_tip, null);

        mWindowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);

        mLayoutParams = new WindowManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSLUCENT
        );

        mLayoutParams.gravity = Gravity.TOP;
        mLayoutParams.x = 0;
        mLayoutParams.y = 0;
    }
}
