package com.xinqiupark.technology;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SvgActivity extends AppCompatActivity {

    @BindView(R.id.iv_image)
    ImageView mIvImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_svg);
        ButterKnife.bind(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Animatable animatable= (Animatable) mIvImage.getDrawable();
        animatable.start();
    }
}
