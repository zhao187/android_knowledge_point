package com.xinqiupark.technology.ui.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.receiver.NotificationReceiver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends AppCompatActivity {

    private NotificationReceiver mNotificationReceiver = null;

    @OnClick(R.id.btn_notification)
    public void onNotificationClick() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("约吗?");
        builder.setContentText("下午一起喝个酒");
        builder.setTicker("Nihao");
        builder.setWhen(System.currentTimeMillis());

        //创建意图
        Intent intent = new Intent(this, ImageLoaderActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        builder.setDefaults(Notification.DEFAULT_SOUND);//设置声音
        builder.setDefaults(Notification.DEFAULT_LIGHTS);//设置指示灯
        builder.setDefaults(Notification.DEFAULT_VIBRATE);//设置震动
        builder.setDefaults(Notification.DEFAULT_ALL);//设置全部


        final Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        //创建通知管理者
        NotificationManager manager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        assert manager != null;
        manager.notify(1, notification);
    }

    @OnClick(R.id.btn_custom_notification)
    public void onCuNotificationClick() {
        Notification.Builder builder = new Notification.Builder(this);

        builder.setTicker("hello");
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setDefaults(Notification.DEFAULT_ALL);

        Intent intent = new Intent(this, ImageLoaderActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews remoteViews = new RemoteViews(getPackageName()
                , R.layout.layout_notification);
        remoteViews.setTextViewText(R.id.tv_music_name, "像我这样的人");
        remoteViews.setTextViewText(R.id.tv_music_author, "毛不易");
        remoteViews.setImageViewResource(R.id.iv_icon, R.drawable.icon_music);

        Intent btnNextIntent = new Intent(NotificationReceiver.ACTION_2);
        PendingIntent btnNextPI = PendingIntent.getBroadcast(this, 0,
                btnNextIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btn_next_music, btnNextPI);

        builder.setContent(remoteViews);
        builder.setContentIntent(pendingIntent);
        final Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        //创建通知管理者
        NotificationManager manager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        assert manager != null;
        manager.notify(1, notification);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifaction);
        ButterKnife.bind(this);

        initNotification();
    }

    /**
     * 判断权限通知是否开启
     */
    protected void initNotification() {
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        boolean isOpened = manager.areNotificationsEnabled();

        if (!isOpened) {
            // 根据isOpened结果，判断是否需要提醒用户跳转AppInfo页面，去打开App通知权限
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", this.getApplication().getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //注册广播
        mNotificationReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NotificationReceiver.ACTION_2);
        registerReceiver(mNotificationReceiver, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(mNotificationReceiver);
    }
}
