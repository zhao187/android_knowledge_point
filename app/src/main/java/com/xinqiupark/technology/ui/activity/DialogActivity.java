package com.xinqiupark.technology.ui.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.dialog.CustomDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogActivity extends AppCompatActivity {

    private CustomDialog mDialog;

    @OnClick(R.id.btn_custom_dialog)
    public void onCustomClick() {
        mDialog.show();
    }

    @OnClick(R.id.btn_different_dialog)
    public void onDifferentDialog()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this)
                        .setMessage(" String oldApkPath = Environment.getExternalStorageDirectory().getAbsolutePath() + \"/patch/xinqiu.apk\";\n" +
                                "        String newApkPath = Environment.getExternalStorageDirectory().getAbsolutePath() + \"/patch/xinqiu_new.apk\";\n" +
                                "        String patchPath = Environment.getExternalStorageDirectory().getAbsolutePath() + \"/patch/xinqiudiff.patch\";\n" +
                                "        Boolean isSuccess = BigNews.make(oldApkPath, newApkPath, patchPath);\n" +
                                "\n" +
                                "        if (isSuccess) {\n" +
                                "            Toast.makeText(this, \"差分包合成成功\", Toast.LENGTH_SHORT)\n" +
                                "                    .show();\n" +
                                "        }")
                        .setCancelable(true)
                        .setNegativeButton("取消",null);
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        ButterKnife.bind(this);

        initView();
    }

    /**
     * 初始化
     */
    private void initView() {
        mDialog = new CustomDialog.Builder(this)
                .cancelTouchOut(true)
                .heightdp(240)
                .widthdp(240)
                .style(R.style.Dialog)
                .view(R.layout.dialog_identitychange)
                .addViewOnclick(R.id.tv_qq, view -> {
                    System.out.println("-->>QQ登录");
                    mDialog.dismiss();
                })
                .addViewOnclick(R.id.tv_alipay,view->{
                    System.out.println("-->>支付宝登录");
                    mDialog.dismiss();
                })
                .addViewOnclick(R.id.btn_confirm,view->{
                    System.out.println("-->>确定按钮");
                    mDialog.dismiss();
                })
                .build();
    }
}
