package com.xinqiupark.technology.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PathMeasure;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.activity.BezierActivity;
import com.xinqiupark.technology.ui.activity.CoordinatorLayout2Activity;
import com.xinqiupark.technology.ui.activity.DanmakuActivity;
import com.xinqiupark.technology.ui.activity.DownLoadActivity;
import com.xinqiupark.technology.ui.activity.PaletteActivity;
import com.xinqiupark.technology.ui.activity.ProgressRingActivity;
import com.xinqiupark.technology.ui.activity.RoundImageActivity;
import com.xinqiupark.technology.ui.activity.ShaderActivity;
import com.xinqiupark.technology.ui.activity.SnapHelperActivity;
import com.xinqiupark.technology.ui.activity.ToastActivity;
import com.xinqiupark.technology.ui.adapter.ListAdapter;
import com.zaaach.citypicker.CityPickerActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static android.support.v7.widget.DividerItemDecoration.HORIZONTAL;
import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/31
 * desc:自定义
 */
public class CustomFragment extends Fragment {
    @BindView(R.id.tv_header_title)
    TextView mTvTitle;
    @BindView(R.id.rcv_data)
    RecyclerView mRcvData;

    private String mTitle;

    private static final int REQUEST_CODE_PICK_CITY = 0;


    private static final String[] TITLE = {
            "城市选择控件",
            "图片取色并让图片融入背景色",
            "自定义环形进度条",
            "图片相关常用方式",
            "SnapHelper设置RecyclerView对其方式",
            "Toast自定义使用",
            "CoordinatorLayout打造各种炫酷的效果",
            "自定义进度条",
            "Shader自定义",
            "弹幕视频播放器",
            "贝塞尔曲线"
    };

    private static final ArrayList<Activity> mDestinations=new ArrayList<Activity>()
    {
        {
            add(new CityPickerActivity());
            add(new PaletteActivity());
            add(new ProgressRingActivity());
            add(new RoundImageActivity());
            add(new SnapHelperActivity());
            add(new ToastActivity());
            add(new CoordinatorLayout2Activity());
            add(new DownLoadActivity());
            add(new ShaderActivity());
            add(new DanmakuActivity());
            add(new BezierActivity());
        }
    };
    private ListAdapter mListAdapter = new ListAdapter(R.layout.layout_list_item, Arrays.asList(TITLE));

    public static CustomFragment newInstance(String title) {
        CustomFragment fragment = new CustomFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString("title");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_custom, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTvTitle.setText(mTitle);

        mRcvData.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mRcvData.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), VERTICAL));
        mRcvData.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), HORIZONTAL));
        mRcvData.setAdapter(mListAdapter);

        mListAdapter.setOnItemClickListener((adapter, view, position) -> {
            if(position==0) {
                startActivityForResult(new Intent(getActivity(), CityPickerActivity.class),
                        REQUEST_CODE_PICK_CITY);
            }
            else
            {
                startActivity(new Intent(getActivity(),mDestinations.get(position).getClass()));
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_CITY && resultCode == RESULT_OK){
            if (data != null){
                String city = data.getStringExtra(CityPickerActivity.KEY_PICKED_CITY);
                Toast.makeText(getActivity(), "当前选择：" + city, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
