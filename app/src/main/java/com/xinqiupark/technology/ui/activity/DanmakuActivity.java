package com.xinqiupark.technology.ui.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.xinqiupark.technology.R;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import master.flame.danmaku.controller.DrawHandler;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDanmakus;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.ui.widget.DanmakuView;

public class DanmakuActivity extends AppCompatActivity {

    @BindView(R.id.video_view)
    VideoView mVideoView;
    @BindView(R.id.danmuku_view)
    DanmakuView mDanmukuView;
    @BindView(R.id.et_text)
    EditText mEtText;
    @BindView(R.id.send)
    Button mSend;
    @BindView(R.id.opeartor_layout)
    LinearLayout mOpeartorLayout;

    private boolean showDanmaku;

    private DanmakuContext mDanmakuContext;


    private BaseDanmakuParser mBaseDanmakuParser = new BaseDanmakuParser() {
        @Override
        protected IDanmakus parse() {
            return new Danmakus();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danmaku);
        ButterKnife.bind(this);

        mVideoView.setVideoPath(Environment.getExternalStorageDirectory() + "/VideoWorld/download/myvideo.mp4");
        mVideoView.start();

        mDanmukuView.setDrawingCacheEnabled(true);
        mDanmukuView.setCallback(new DrawHandler.Callback() {
            @Override
            public void prepared() {
                showDanmaku = true;
                mDanmukuView.start();
                generateSomeDanmaku();
            }

            @Override
            public void updateTimer(DanmakuTimer timer) {

            }

            @Override
            public void danmakuShown(BaseDanmaku danmaku) {

            }

            @Override
            public void drawingFinished() {

            }
        });

        mDanmakuContext = DanmakuContext.create();
        mDanmukuView.prepare(mBaseDanmakuParser, mDanmakuContext);

        mDanmukuView.setOnClickListener(view->{
            if(mOpeartorLayout.getVisibility()==View.GONE)
            {
                mOpeartorLayout.setVisibility(View.VISIBLE);
            }
            else
            {
                mOpeartorLayout.setVisibility(View.GONE);
            }
        });

        mSend.setOnClickListener(view->{
            String content=mEtText.getText().toString().trim();
            if(!TextUtils.isEmpty(content))
            {
                addDanMaKu(content,true);
                mEtText.setText("");
            }
        });

        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(l->{
            if(l==View.SYSTEM_UI_FLAG_VISIBLE)
            {
                onWindowFocusChanged(true);
            }
        });
    }

    private void generateSomeDanmaku() {
        new Thread(() -> {
            while (showDanmaku) {
                int time = new Random().nextInt(300);
                String content = "" + time + time;
                addDanMaKu(content, false);

                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void addDanMaKu(String content, boolean withBorder) {
        BaseDanmaku danmaku = mDanmakuContext.mDanmakuFactory
                .createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
        danmaku.textColor = Color.WHITE;
        danmaku.padding = 5;
        danmaku.textSize = sp2px(20);
        danmaku.setTime(mDanmukuView.getCurrentTime());
        danmaku.text = content;
        if (withBorder)
            danmaku.borderColor = Color.GREEN;
        mDanmukuView.addDanmaku(danmaku);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && Build.VERSION.SDK_INT >= 19) {
            final View decorView = getWindow().getDecorView();

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    public int sp2px(float spValue) {
        float fontScale = getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue + fontScale + 0.5);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDanmukuView != null && mDanmukuView.isPressed()) {
            mDanmukuView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDanmukuView != null && mDanmukuView.isPressed()
                && mDanmukuView.isPaused()
                ) {
            mDanmukuView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        showDanmaku = false;
        if (mDanmukuView != null) {
            mDanmukuView.release();
            mDanmukuView = null;
        }
    }
}
