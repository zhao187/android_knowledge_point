package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.VpImgAdapter;
import com.xinqiupark.technology.view.CardTransformer;
import com.xinqiupark.technology.view.GalleryTransformer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewPagerActivity extends AppCompatActivity {

    @BindView(R.id.vp_main_pager)
    ViewPager mVpMainPager;
    @BindView(R.id.vp_card_pager)
    ViewPager mVpCardPager;

    private ArrayList<Integer> mImages = new ArrayList<Integer>() {{
        add(R.drawable.icon_tu1);
        add(R.drawable.icon_tu2);
        add(R.drawable.icon_tu3);
        add(R.drawable.icon_tu4);
    }};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        ButterKnife.bind(this);

        VpImgAdapter mVpImgAdapter = new VpImgAdapter(this, mImages);
        mVpMainPager.setOffscreenPageLimit(3);
        mVpMainPager.setPageMargin(40);
        mVpMainPager.setAdapter(mVpImgAdapter);

        mVpMainPager.setPageTransformer(true, new GalleryTransformer());


        mVpCardPager.setOffscreenPageLimit(4);
        mVpCardPager.setAdapter(mVpImgAdapter);
        mVpCardPager.setPageTransformer(true,new CardTransformer());
    }
}
