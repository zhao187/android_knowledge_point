package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.ListAdapter;

import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.VERTICAL;

public class CoordinatorLayoutActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private static final String[] TITLE = {
            "Xml解析",
            "Jsoup进行网络html标签解析",
            "通知",
            "文件下载",
            "合并差分包",
            "Android截图",
            "Android图片相册",
            "Glide图片操作",
            "二维码扫描",
            "引导图",
            "svg图像",
            "TextView的操作",
            "RxJava的相关操",
            "dialog的封装",
            "android非空等格式验证框架",
            "揭露动画",
            "StackView控件",
            "缓存相关操作",
            "ViewPager相关操作"
    };

    private ListAdapter mListAdapter = new ListAdapter(R.layout.layout_list_item, Arrays.asList(TITLE));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinator_layout);
        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), VERTICAL));
        mRecyclerView.setAdapter(mListAdapter);
    }
}
