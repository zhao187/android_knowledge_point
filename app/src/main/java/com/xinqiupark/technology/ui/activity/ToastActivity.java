package com.xinqiupark.technology.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
动画基本可以分为 Animator 和 Animation 两大类，
而 View.animate() 返回的是一个 ViewPropertyAnimator 类型的对象，这个类并没有继承自任何类

最后，就来进行一下总结：

View.animate() 这种方式实现的动画其实是 ViewPropertyAnimator 动画。

ViewPropertyAnimator 并不是一种动画，它没有继承自 Animator 或者 Animation，它其实只是一个封装类，将常用的动画封装起来，对外提供方便使用的接口，内部借助 ValueAnimator 机制。

ViewPropertyAnimator 动画支持自动启动动画，如果外部没有明确调用了 start()，那么内部会安排一个 Runnable 操作，最迟在下一帧内被执行，这个 Runnable 会去启动动画。

当然，如果外部手动调用了 start()，那么自动启动动画就没意义了，内部会自己将其取消掉。

ViewPropertyAnimator 对外提供的使用动画的接口非常方便，如 scaleX() 表示 x 的缩放动画，alpha() 表示透明度动画，而且支持链式调用。

由于支持链式调用，所以它支持一系列动画一起开始，一起执行，一起结束。那么当这一系列动画还没执行完又重新发起了另一系列的动画时，此时两个系列动画就需要分成两组，每一组动画互不干扰，可以同时执行。

但如果同一种类型的动画，如 SCALE_X，在同一帧内分别在多组里都存在，如果都同时运行的话，View 的状态会变得很错乱，所以 ViewPropertyAnimator 规定，同一种类型的动画在同一时刻只能有一个在运行。

也就是说，多组动画可以处于并行状态，但是它们内部的动画是没有交集的，如果有交集，比如 SCALE_X 动画已经在运行中了，但是外部又新设置了一个新的 SCALE_X 动画，那么之前的那个动画就会被取消掉，新的 SCALE_X 动画才会加入新的一组动画中。

由于内部是借助 ValueAnimator 机制，所以在每一帧内都可以接收到回调，在回调中取得 ValueAnimator 计算出的当前帧的动画进度。

取出当前帧的动画进度后，就可以遍历跟当前 ValueAnimator 绑定的那一组动画里所有的动画，分别根据每一个动画保存的信息，来计算出当前帧这个动画的属性值，然后调用 View 的 mRenderNode 对象的 setXXX 方法来修改属性值，达到动画效果。
 */
public class ToastActivity extends AppCompatActivity {

    private ToastUtil mToastUtil;

    @BindView(R.id.btn_change_color)
    public Button mBtnChangColor;

    @OnClick(R.id.btn_change_color)
    public void onChangeColor() {
        mToastUtil.Short(this, "自定义message字体颜色和背景").setToastBackground(Color.WHITE
                , R.drawable.toast_radius).show();
    }

    @OnClick(R.id.btn_change_bg)
    public void onChangeBg()
    {
        ImageView toastImage = new ImageView(getApplicationContext());
        toastImage.setImageResource(R.mipmap.ic_launcher);
        mToastUtil.Short(this, "自定义背景").setToastBackground(Color.WHITE
                , R.drawable.toast_radius).addView(toastImage,1).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
        ButterKnife.bind(this);

        mToastUtil = new ToastUtil();

        mBtnChangColor.animate()
                .alpha(0.5f)
                .scaleX(1.2f)
                .scaleY(1.2f)
                .setDuration(1000).start();
    }
}
