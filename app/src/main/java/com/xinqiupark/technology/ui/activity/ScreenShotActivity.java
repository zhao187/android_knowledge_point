package com.xinqiupark.technology.ui.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xinqiupark.technology.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScreenShotActivity extends AppCompatActivity {

    @BindView(R.id.iv_capture_screen)
    ImageView mIvCaptureScreen;
    @BindView(R.id.ll_content)
    LinearLayout mLlContent;

    @OnClick({R.id.btn_capture, R.id.btn_capture_type1, R.id.btn_capture_type2})
    public void onCaptureClick(View view) {
        switch (view.getId()) {
            case R.id.btn_capture:
                View dView = getWindow().getDecorView();

                dView.setDrawingCacheEnabled(true);
                dView.buildDrawingCache();
                Bitmap bitmap = Bitmap.createBitmap(dView.getDrawingCache());
                if (bitmap != null) {
                    mIvCaptureScreen.setImageBitmap(bitmap);
                }
                dView.setDrawingCacheEnabled(false);
                break;
            case R.id.btn_capture_type1:
                View llView = mLlContent;
                llView.setDrawingCacheEnabled(true);
                llView.buildDrawingCache();
                Bitmap bitmap1 = Bitmap.createBitmap(llView.getDrawingCache());

                if (bitmap1 != null) {
                    mIvCaptureScreen.setImageBitmap(bitmap1);
                }

                llView.setDrawingCacheEnabled(false);
                break;
            case R.id.btn_capture_type2:
                View llView2 = mLlContent;
                Bitmap bitmap2 = Bitmap.createBitmap(llView2.getWidth(), llView2.getHeight(), Bitmap.Config.ARGB_8888);
                //将以上创建的Bitmap指定为要绘制的Bitmap作为参数创建画布
                Canvas canvas = new Canvas(bitmap2);
                //将View绘制在画布之上
                llView2.draw(canvas);

                mIvCaptureScreen.setImageBitmap(bitmap2);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_shot);
        ButterKnife.bind(this);
    }
}
