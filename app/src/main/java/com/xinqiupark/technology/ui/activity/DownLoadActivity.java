package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.arialyy.annotations.Download;
import com.arialyy.aria.core.Aria;
import com.arialyy.aria.core.download.DownloadTask;
import com.xinqiupark.technology.R;
import com.xinqiupark.technology.view.HorizontalProgressBar;
import com.xinqiupark.technology.view.HorizontalProgressBarWithNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DownLoadActivity extends AppCompatActivity {

    private static final String DOWNLOAD_URL =
            "http://static.gaoshouyou.com/d/d4/4f/d6d48db3794fb9ecf47e83c346570881.apk";

    @BindView(R.id.progressBar)
    HorizontalProgressBarWithNumber mPb;
    //@Bind(R.id.start) Button mStart;
    //@Bind(R.id.stop) Button mStop;
    //@Bind(R.id.cancel) Button mCancel;
    @BindView(R.id.size)
    TextView mSize;
    @BindView(R.id.speed)
    TextView mSpeed;
    @BindView(R.id.pb_custom)
    HorizontalProgressBar mPbCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_load);

        ButterKnife.bind(this);

        Aria.download(this).register();

        initView();
    }

    private void initView() {
        mPbCustom.setProgressWithAnimation(80)
                .setProgressListener(new HorizontalProgressBar.ProgressListener() {
                    @Override
                    public void currentProgressListener(float currentProgress) {
                        System.out.println("-->>"+currentProgress);
                    }
                });
    }

    @OnClick({R.id.start, R.id.stop, R.id.cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start:
                Aria.download(this)
                        .load(DOWNLOAD_URL)
                        .setDownloadPath(Environment.getExternalStorageDirectory().getPath() + "/aria_test.apk")
                        .start();
                break;
            case R.id.stop:
                Aria.download(this).load(DOWNLOAD_URL).pause();
                break;
            case R.id.cancel:
                Aria.download(this).load(DOWNLOAD_URL).cancel();
                break;
        }
    }

    //在这里处理任务执行中的状态，如进度进度条的刷新
    @Download.onTaskRunning
    protected void running(DownloadTask task) {
        int p = task.getPercent();  //任务进度百分比
        String speed = task.getConvertSpeed();  //转换单位后的下载速度，单位转换需要在配置文件中打开

        mSpeed.setText(task.getConvertSpeed());
        mPb.setProgress(task.getPercent());
    }

    @Download.onTaskComplete
    void taskComplete(DownloadTask task) {
        //在这里处理任务完成的状态
    }
}
