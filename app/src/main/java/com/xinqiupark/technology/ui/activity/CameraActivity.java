package com.xinqiupark.technology.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.xinqiupark.technology.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_surfaceView)
    public void onBtnSurfaceClick() {
        startActivity(new Intent(this, SurfaceViewActivity.class));
    }

    @OnClick(R.id.btn_textureview)
    public void onBtnTextureViewClick()
    {
        startActivity(new Intent(this,TextureViewActivity.class));
    }
}
