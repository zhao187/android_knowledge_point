package com.xinqiupark.technology.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/3
 * desc:图片适配器
 */
public class StackImageAdapter extends BaseAdapter {
    private ArrayList<Integer> mImages;
    private Context mContext;

    public StackImageAdapter(ArrayList<Integer> mImages, Context context){
        this.mImages = mImages;
        mContext = context;
    }
    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public Object getItem(int position) {
        return mImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(mImages.get(position));
        return imageView;
    }
}
