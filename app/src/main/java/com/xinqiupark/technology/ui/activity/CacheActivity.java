package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.util.CacheUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 缓存操作
 */
public class CacheActivity extends AppCompatActivity {

    @BindView(R.id.tv_content)
    TextView mTvContent;

    private CacheUtils mCacheUtils;

    @OnClick(R.id.btn_normal_cache)
    public void onNormalCacheClick()
    {
        StringBuilder sb=new StringBuilder();
        sb.append("-->>").append(mCacheUtils.getString("test_key1")).append("-->>").append(mCacheUtils.getString("test_key2", "defaultValue")).append("-->>").append(mCacheUtils.getString("test_key3", "defaultValue"));

        mTvContent.setText(sb.toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cache);
        ButterKnife.bind(this);

        mCacheUtils = CacheUtils.getInstance();
        mCacheUtils.put("test_key1", "test value");
        mCacheUtils.put("test_key2", "test value", 10);// 保存10s，10s后获取返回null
        mCacheUtils.put("test_key3", "test value", 12 * CacheUtils.HOUR);// 保存12小时，12小时后获取返回null
    }
}
