package com.xinqiupark.technology.ui.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xinqiupark.technology.R;

import java.util.List;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/4
 * desc:
 */
public class SnapHelperAdapter extends BaseQuickAdapter<Integer,BaseViewHolder> {
    public SnapHelperAdapter(int layoutResId, @Nullable List<Integer> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Integer item) {
        helper.setImageResource(R.id.iv_image,item);
    }
}
