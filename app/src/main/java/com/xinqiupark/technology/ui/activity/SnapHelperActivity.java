package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.SnapHelperAdapter;
import com.xinqiupark.technology.view.GallerySnapHelper;
import com.xinqiupark.technology.view.SpacesItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SnapHelperActivity extends AppCompatActivity {

    @BindView(R.id.rcv_data)
    RecyclerView mRcvData;


    private ArrayList<Integer> mImages = new ArrayList<Integer>() {{
        add(R.drawable.icon_tu1);
        add(R.drawable.icon_tu2);
        add(R.drawable.icon_tu3);
        add(R.drawable.icon_tu4);
        add(R.drawable.icon_tu1);
        add(R.drawable.icon_tu2);
        add(R.drawable.icon_tu3);
        add(R.drawable.icon_tu4);
        add(R.drawable.icon_tu1);
        add(R.drawable.icon_tu2);
        add(R.drawable.icon_tu3);
        add(R.drawable.icon_tu4);
    }};

    private SnapHelperAdapter mAdapter=new SnapHelperAdapter(R.layout.layout_snap_item,mImages);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap_helper);
        ButterKnife.bind(this);

        mRcvData.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.HORIZONTAL,false));
        mRcvData.addItemDecoration(new SpacesItemDecoration(8));
        mRcvData.setAdapter(mAdapter);

//        new LinearSnapHelper().attachToRecyclerView(mRcvData);

        new GallerySnapHelper().attachToRecyclerView(mRcvData);
    }
}
