package com.xinqiupark.technology.ui.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.util.BlurUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoundImageActivity extends AppCompatActivity {

    @BindView(R.id.iv_image)
    ImageView mIvImage;
    @BindView(R.id.iv_image2)
    ImageView mIvImage2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round_image);
        ButterKnife.bind(this);

        //图像进行模糊处理
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icon_tu1);
        mIvImage.setImageBitmap(BlurUtils.rsBlur(this, bmp, 25,0.5f));

        Bitmap bmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.icon_tu2);
        mIvImage2.setImageBitmap(BlurUtils.fastBlur(bmp2,0.5f,45));
    }
}
