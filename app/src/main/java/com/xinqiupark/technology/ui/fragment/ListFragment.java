package com.xinqiupark.technology.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.ListAdapter;

import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/17
 * desc:
 */
public class ListFragment extends Fragment {

    @BindView(R.id.rcv_data)
    RecyclerView mRcvData;

    private static final String[] TITLE = {
            "Xml解析",
            "Jsoup进行网络html标签解析",
            "通知",
            "文件下载",
            "合并差分包",
            "Android截图",
            "Android图片相册",
            "Glide图片操作",
            "二维码扫描",
            "引导图",
            "svg图像",
            "TextView的操作",
            "RxJava的相关操",
            "dialog的封装",
            "android非空等格式验证框架",
            "揭露动画",
            "StackView控件",
            "缓存相关操作",
            "ViewPager相关操作"
    };

    private ListAdapter mListAdapter = new ListAdapter(R.layout.layout_list_item, Arrays.asList(TITLE));

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }

    /**
     * 初始化视图
     */
    private void initView() {
        mRcvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRcvData.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), VERTICAL));
        mRcvData.setAdapter(mListAdapter);
    }
}
