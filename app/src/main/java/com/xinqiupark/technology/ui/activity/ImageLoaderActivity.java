package com.xinqiupark.technology.ui.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.AbsListView;
import android.widget.GridView;

import com.xinqiupark.technology.BaseActivity;
import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.adapter.ImageAdapter;
import com.xinqiupark.technology.entity.Images;

import java.util.Arrays;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/22
 * desc:图片加载
 */
public class ImageLoaderActivity extends BaseActivity implements AbsListView.OnScrollListener {
    private GridView mGridView;

    private boolean mIsGridViewIdle=true;

    private boolean mCanGetBitmapFromNetWork=true;

    private ImageAdapter imageAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imgae_loader);

        isNetWifi();

        mGridView=findViewById(R.id.gridview1);
        imageAdapter=new ImageAdapter(this);
        imageAdapter.setGridViewIdle(mIsGridViewIdle);
        imageAdapter.setCanGetBitmapFromNetWork(mCanGetBitmapFromNetWork);

        mGridView.setAdapter(imageAdapter);

        //第二种viewgroup item设置动画效果
//        Animation animation= AnimationUtils.loadAnimation(this,R.anim.anim_item);
//        LayoutAnimationController controller=new LayoutAnimationController(animation);
//        controller.setDelay(0.5f);
//        controller.setOrder(LayoutAnimationController.ORDER_NORMAL);
//        mGridView.setLayoutAnimation(controller);controller

        mGridView.setOnScrollListener(this);



        imageAdapter.setUrList(Arrays.asList(Images.imageThumbUrls));
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState== AbsListView.OnScrollListener.SCROLL_STATE_IDLE)
        {
            mIsGridViewIdle=true;
            imageAdapter.notifyDataSetChanged();
        }
        else
        {
            mIsGridViewIdle=false;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) { }

    private void isNetWifi()
    {
        boolean mIsWifi=false;

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("初次使用会从网络中下载大概5MB的图片，请确认下载吗?");
        builder.setTitle("注意");
        builder.setPositiveButton("是", (dialog, which) -> {
           mCanGetBitmapFromNetWork=true;
           imageAdapter.notifyDataSetChanged();
        });
        builder.setNegativeButton("否",null);
        builder.show();
    }
}
