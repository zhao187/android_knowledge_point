package com.xinqiupark.technology.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.xinqiupark.technology.SvgActivity;
import com.xinqiupark.technology.reveal_anim.RevealOneActivity;
import com.xinqiupark.technology.ui.activity.CacheActivity;
import com.xinqiupark.technology.ui.activity.CameraActivity;
import com.xinqiupark.technology.ui.activity.DialogActivity;
import com.xinqiupark.technology.ui.activity.DownLoadActivity;
import com.xinqiupark.technology.ui.activity.GlideActivity;
import com.xinqiupark.technology.ui.activity.JsoupActivity;
import com.xinqiupark.technology.ui.activity.LockScreenActivity;
import com.xinqiupark.technology.ui.activity.LruCacheActivity;
import com.xinqiupark.technology.ui.activity.NotificationActivity;
import com.xinqiupark.technology.ui.activity.PhotoActivity;
import com.xinqiupark.technology.ui.activity.RxJavaActivity;
import com.xinqiupark.technology.ui.activity.GlideViewPagerActivity;
import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.activity.SaripaarActivity;
import com.xinqiupark.technology.ui.activity.ScannerActivity;
import com.xinqiupark.technology.ui.activity.ScreenShotActivity;
import com.xinqiupark.technology.ui.activity.StackViewActivity;
import com.xinqiupark.technology.ui.activity.TextViewActivity;
import com.xinqiupark.technology.ui.activity.ToolsActivity;
import com.xinqiupark.technology.ui.activity.ViewPagerActivity;
import com.xinqiupark.technology.ui.activity.XmlParseActivity;
import com.xinqiupark.technology.ui.adapter.ListAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.DividerItemDecoration.*;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/31
 * desc:基础知识
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.tv_header_title)
    TextView mTvTitle;

    @BindView(R.id.rcv_data)
    RecyclerView mRecyclerView;

    private String mTitle;

    private static final String[] TITLE = {
            "Xml解析",
            "Jsoup进行网络html标签解析",
            "通知",
            "文件下载",
            "合并差分包",
            "Android截图",
            "Android图片相册",
            "Glide图片操作",
            "二维码扫描",
            "引导图",
            "svg图像",
            "TextView的操作",
            "RxJava的相关操",
            "dialog的封装",
            "android非空等格式验证框架",
            "揭露动画",
            "StackView控件",
            "缓存相关操作",
            "ViewPager相关操作",
            "相机预览",
            "LruCache缓存示例",
            "tools简单使用",
            "Android仿QQ实现锁屏消息提醒"
    };

    private static final ArrayList<Activity> mDestinations=new ArrayList<Activity>()
    {
        {
            add(new XmlParseActivity());
            add(new JsoupActivity());
            add(new NotificationActivity());
            add(new DownLoadActivity());
            add(new DialogActivity());
            add(new ScreenShotActivity());
            add(new PhotoActivity());
            add(new GlideActivity());
            add(new ScannerActivity());
            add(new GlideViewPagerActivity());
            add(new SvgActivity());
            add(new TextViewActivity());
            add(new RxJavaActivity());
            add(new DialogActivity());
            add(new SaripaarActivity());
            add(new RevealOneActivity());
            add(new StackViewActivity());
            add(new CacheActivity());
            add(new ViewPagerActivity());
            add(new CameraActivity());
            add(new LruCacheActivity());
            add(new ToolsActivity());
            add(new LockScreenActivity());
        }
    };


    private ListAdapter mListAdapter = new ListAdapter(R.layout.layout_list_item, Arrays.asList(TITLE));

    public static HomeFragment newInstance(String title) {
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString("title");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initListener();
    }

    /**
     * 初始化监听器
     */
    private void initListener() {
        mListAdapter.setOnItemClickListener((adapter, view, position) -> {
            startActivity(new Intent(getActivity(),mDestinations.get(position).getClass()));
        });
    }

    /**
     * 初始化视图
     */
    private void initView() {
        mTvTitle.setText(mTitle);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), VERTICAL));
        mRecyclerView.setAdapter(mListAdapter);
    }
}
