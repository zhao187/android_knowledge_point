package com.xinqiupark.technology.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.xinqiupark.technology.R;
import com.zhangke.qrcodeview.QRCodeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScannerActivity extends AppCompatActivity {

    @BindView(R.id.qr_code_view)
    QRCodeView mQrCodeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        ButterKnife.bind(this);

        mQrCodeView.setOnQRCodeListener(result -> {
            Toast.makeText(this, result.getText(), Toast.LENGTH_SHORT).show();
        });

//        try {
//            QRCodeUtil.createQRCode("赵宏图");  二维码工具类
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mQrCodeView.startPreview();
    }

    @Override
    protected void onPause() {
        mQrCodeView.stopPreview();
        super.onPause();
    }
}
