package com.xinqiupark.technology.ui.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.xinqiupark.technology.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TextViewActivity extends AppCompatActivity {

    @BindView(R.id.tv_text1)
    TextView mTvText1;
    @BindView(R.id.tv_text2)
    TextView mTvText2;
    @BindView(R.id.tv_text3)
    TextView mTvText3;
    @BindView(R.id.tv_text4)
    TextView mTvText4;
    @BindView(R.id.tv_text5)
    TextView mTvText5;
    @BindView(R.id.tv_text6)
    TextView mTvText6;
    @BindView(R.id.tv_text7)
    TextView mTvText7;
    @BindView(R.id.tv_text8)
    TextView mTvText8;
    @BindView(R.id.tv_text9)
    TextView mTvText9;
    @BindView(R.id.tv_text10)
    TextView mTvText10;
    @BindView(R.id.tv_text11)
    TextView mTvText11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);
        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        //1.ForegroundColorSpan
        SpannableString spannableString1 = new SpannableString("设置文字的前景色为淡蓝色");
        ForegroundColorSpan colorSpan1 = new ForegroundColorSpan(Color.parseColor("#0099EE"));
        spannableString1.setSpan(colorSpan1, 9, spannableString1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvText1.setText(spannableString1);

        //2.BackgroundColorSpan
        SpannableString spannableString2 = new SpannableString("设置文字的前景色为淡蓝色");
        BackgroundColorSpan colorSpan2 = new BackgroundColorSpan(Color.parseColor("#0099EE"));
        spannableString2.setSpan(colorSpan2, 9, spannableString2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvText2.setText(spannableString2);

        //3.设置文字大小不规则
        SpannableString spannableString3 = new SpannableString("万丈高楼平地起");
        RelativeSizeSpan sizeSpan01 = new RelativeSizeSpan(1.2f);
        RelativeSizeSpan sizeSpan02 = new RelativeSizeSpan(1.4f);
        RelativeSizeSpan sizeSpan03 = new RelativeSizeSpan(1.6f);
        RelativeSizeSpan sizeSpan04 = new RelativeSizeSpan(1.8f);
        RelativeSizeSpan sizeSpan05 = new RelativeSizeSpan(1.6f);
        RelativeSizeSpan sizeSpan06 = new RelativeSizeSpan(1.4f);
        RelativeSizeSpan sizeSpan07 = new RelativeSizeSpan(1.2f);
        spannableString3.setSpan(sizeSpan01, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan02, 1, 2, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan03, 2, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan04, 3, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan05, 4, 5, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan06, 5, 6, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString3.setSpan(sizeSpan07, 6, 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        mTvText3.setText(spannableString3);

        //4.设置文字中划线 StrikethroughSpan
        SpannableString spannableString4 = new SpannableString("为文字设置删除线");
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        spannableString4.setSpan(strikethroughSpan, 5, spannableString4.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText4.setText(spannableString4);

        //5.设置下划线 UnderlineSpan
        SpannableString spannableString5 = new SpannableString("为文字设置下划线");
        UnderlineSpan underlineSpan = new UnderlineSpan();
        spannableString5.setSpan(underlineSpan, 5, spannableString5.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText5.setText(spannableString5);

        //6.设置上标 SuperscriptSpan
        SpannableString spannableString6 = new SpannableString("为文字设置上标");
        SuperscriptSpan superscriptSpan = new SuperscriptSpan();
        spannableString6.setSpan(superscriptSpan, 5, spannableString6.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText6.setText(spannableString6);

        //7.设置 下标 SubscriptSpan
        SpannableString spannableString7=new SpannableString("为文字设置下标");
        SubscriptSpan subscriptSpan=new SubscriptSpan();
        spannableString7.setSpan(subscriptSpan, 5, spannableString7.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText7.setText(spannableString7);

        //8.设置风格（粗体、斜体）StyleSpan
        SpannableString spannableString8 = new SpannableString("为文字设置粗体、斜体风格");
        StyleSpan styleSpan_B = new StyleSpan(Typeface.BOLD);
        StyleSpan styleSpan_I = new StyleSpan(Typeface.ITALIC);
        spannableString8.setSpan(styleSpan_B, 5, 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString8.setSpan(styleSpan_I, 8, 10, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText8.setHighlightColor(Color.parseColor("#36969696"));
        mTvText8.setText(spannableString8);

        //9.在文本中添加图片 ImageSpan
        SpannableString spannableString9 = new SpannableString("在文本中添加表情（表情）");
        Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
        drawable.setBounds(0,0,42,42);
        ImageSpan imageSpan=new ImageSpan(drawable);
        spannableString9.setSpan(imageSpan, 6, 8, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText9.setText(spannableString9);

        //10.为字体添加点击信息
        SpannableString spannableString10 = new SpannableString("为文字设置点击事件");
        MyClickableSpan clickableSpan = new MyClickableSpan("http://www.jianshu.com/users/dbae9ac95c78");
        spannableString10.setSpan(clickableSpan, 5, spannableString10.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTvText10.setMovementMethod(LinkMovementMethod.getInstance());
        mTvText10.setHighlightColor(Color.parseColor("#36969696"));
        mTvText10.setText(spannableString10);

        //11.超链接 文本
        SpannableString spannableString = new SpannableString("为文字设置超链接");
        URLSpan urlSpan = new URLSpan("http://www.jianshu.com/users/dbae9ac95c78");
        spannableString.setSpan(urlSpan, 5, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvText11.setMovementMethod(LinkMovementMethod.getInstance());
        mTvText11.setHighlightColor(Color.parseColor("#36969696"));
        mTvText11.setText(spannableString);

        //12.SpannableStringBuilder
//        SpannableStringBuilder spannableStringBuilder=new SpannableStringBuilder();
    }

    class MyClickableSpan extends ClickableSpan {

        private String content;

        public MyClickableSpan(String content) {
            this.content = content;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
//            ds.setColor(ds.linkColor);            //设置可以点击文本部分的颜色
            ds.setUnderlineText(false);            //设置该文本部分是否显示超链接形式的下划线
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(TextViewActivity.this, content, Toast.LENGTH_SHORT).show();
        }
    }
}
