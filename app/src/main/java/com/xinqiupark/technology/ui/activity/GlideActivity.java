package com.xinqiupark.technology.ui.activity;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.xinqiupark.technology.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.GrayscaleTransformation;

public class GlideActivity extends AppCompatActivity {

    @BindView(R.id.image_view)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide);
        ButterKnife.bind(this);

        //Glide专门给我们提供了预加载的接口
        Glide.with(this)
                .load("http://guolin.tech/book.png")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        //预加载成功
                        System.out.println("-->>预加载图片成功");
                        return true;
                    }
                })
                .preload();
    }

    /*
    加载图片

        DiskCacheStrategy.NONE： 表示不缓存任何内容。
        DiskCacheStrategy.DATA： 表示只缓存原始图片。
        DiskCacheStrategy.RESOURCE： 表示只缓存转换过后的图片。
        DiskCacheStrategy.ALL ： 表示既缓存原始图片，也缓存转换过后的图片。
        DiskCacheStrategy.AUTOMATIC： 表示让Glide根据图片资源智能地选择使用哪一种缓存策略（默认选项）。
     */
    public void loadImage(View view) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.icon_music)//占位图
                .error(R.drawable.icon_error)//错误图片
                .diskCacheStrategy(DiskCacheStrategy.NONE)//缓存策略
                .skipMemoryCache(true)//禁用内存缓存功能
//                .override(200,200);//指定图片大小
                .override(Target.SIZE_ORIGINAL);//原始尺寸 有可能报错OOM
        String url = "http://guolin.tech/book.png";
        Glide.with(this).load(url)
                .apply(options)
                .into(mImageView);
    }

    /*
    加载Gif
     */
    public void loadGifImage(View view) {
        Glide.with(this)
                .asBitmap()//强制转换为静态图
                .load("http://guolin.tech/test.gif")
                .into(mImageView);
    }

    /*
    加载图片到指定位置
     */
    public void loadTargetImage(View view) {
        Glide.with(this)
                .load("http://guolin.tech/book.png")
                .into(simpleTarget);
    }

    SimpleTarget<Drawable> simpleTarget = new SimpleTarget<Drawable>() {
        @Override
        public void onResourceReady(@NonNull Drawable resource, Transition<? super Drawable> transition) {
            mImageView.setImageDrawable(resource);
        }
    };

    /*
    获取图片下载路径
     */
    public void loadPathImage(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = "http://www.guolin.tech/book.png";
                    final Context context = getApplicationContext();
                    FutureTarget<File> target = Glide.with(context)
                            .asFile()
                            .load(url)
                            .submit();
                    final File imageFile = target.get();
                    runOnUiThread(() -> Toast.makeText(context, imageFile.getPath(), Toast.LENGTH_LONG).show());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    /*
    图片转换
          RequestOptions options = new RequestOptions()
        .centerCrop();

        RequestOptions options = new RequestOptions()
        .fitCenter();

        RequestOptions options = new RequestOptions()
        .circleCrop();
     */
    public void loadCenterImage(View view) {
        String url = "http://guolin.tech/book.png";
        RequestOptions options = new RequestOptions()
                .circleCrop();
        Glide.with(this)
                .load(url)
                .apply(options)
                .into(mImageView);
    }

    /*
    图片转换库
     */
    public void loadTranformImage(View view) {
        String url = "http://guolin.tech/book.png";
        RequestOptions options = new RequestOptions()
                .transforms(new BlurTransformation(), new GrayscaleTransformation());
        Glide.with(this)
                .load(url)
                .apply(options)
                .into(mImageView);
    }
}
