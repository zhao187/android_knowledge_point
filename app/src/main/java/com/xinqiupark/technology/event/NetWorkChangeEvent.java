package com.xinqiupark.technology.event;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/20
 * desc:网络改变 事件
 */
public class NetWorkChangeEvent {
    public boolean isConnected;//是否存在网络

    public NetWorkChangeEvent(boolean isConnected) {
        this.isConnected = isConnected;
    }
}
