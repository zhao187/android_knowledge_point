package com.xinqiupark.technology.entity;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/28
 * desc:房子
 */
public class House {

    public House(String communityName, String desc) {
        this.communityName = communityName;
        this.desc = desc;
    }

    private String communityName;
    private String desc;

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "House{" +
                "communityName='" + communityName + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
