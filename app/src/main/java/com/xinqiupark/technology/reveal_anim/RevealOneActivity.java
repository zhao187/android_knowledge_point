package com.xinqiupark.technology.reveal_anim;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.util.CircularAnimUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RevealOneActivity extends AppCompatActivity {


    @BindView(R.id.iv_image)
    ImageView mIvImage;
    @BindView(R.id.btn_spread)
    Button mBtnSpread;
    @BindView(R.id.btn_scale)
    Button mBtnScale;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reveal_one);
        ButterKnife.bind(this);

        mBtnSpread.setOnClickListener(view -> {
            CircularAnimUtil.show(mIvImage);
        });

        mBtnScale.setOnClickListener(view -> {
            CircularAnimUtil.hide(mIvImage);
        });
    }
}
