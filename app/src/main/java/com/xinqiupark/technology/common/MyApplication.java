package com.xinqiupark.technology.common;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.arialyy.aria.core.Aria;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/29
 * desc:全局设置
 */
public class MyApplication extends Application {

    public static Context context = null;


    @Override
    public void onCreate() {
        super.onCreate();

        Aria.init(this);

        context=this;
    }
}
