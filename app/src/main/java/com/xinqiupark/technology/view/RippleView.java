package com.xinqiupark.technology.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/17
 * desc:水波纹效果
 */
public class RippleView extends AppCompatTextView {

    private int mX, mY;
    private int mCurRadius = 0;
    private int DEFAULT_RADIUS = 50;

    private RadialGradient mRadialGradient;

    private Paint mPaint=new Paint();
    private ObjectAnimator mAnimator;


    public RippleView(Context context) {
        super(context);
    }

    public RippleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint.setAntiAlias(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mX != event.getX() || mY != event.getY()) {
            mX = (int) event.getX();
            mY = (int) event.getY();
            setRadius(DEFAULT_RADIUS);
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            //如果不返回true，后续的事件收不到
            return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (mAnimator != null && mAnimator.isRunning()) {
                mAnimator.cancel();
            }
            if (mAnimator == null) {
                //这里第一个对象传递当前对象，在当前对象中设置了setRadius方法，所以这里传递radius
                //每当值变化时就会调用这个setRadius方法
                mAnimator = ObjectAnimator.ofInt(this, "radius", DEFAULT_RADIUS, getWidth());
            }
            mAnimator.setInterpolator(new AccelerateInterpolator());
            mAnimator.setDuration(1000);
            mAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) { }

                @Override
                public void onAnimationEnd(Animator animation) {
                    setRadius(0);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) { }
            });
            mAnimator.start();
        }
        return super.onTouchEvent(event);
    }

    //注意这里的方法名必须是setRadius
    public void setRadius(int radius) {
        mCurRadius = radius;

        if (mCurRadius > 0) {
            mRadialGradient = new RadialGradient(mX, mY, radius, 0x00FFFFFF, 0xFF58FAAC, Shader.TileMode.CLAMP);
            mPaint.setShader(mRadialGradient);
        }
        invalidate();
    }

    public int getRadius() {
        return mCurRadius;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(mX, mY, mCurRadius, mPaint);
    }
}

