package com.xinqiupark.technology.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.View;

import com.xinqiupark.technology.R;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/17
 * desc:CLAMP :
 *  它的意思当要绘制的区间大于图片纹理本身的区间时，多出来的空间位置将被纹理图片的边缘颜色填充。
 * MIRROR:
 * 这个模式能够让纹理以镜像的方式在X和Y方向复制
 * REPEAT:
 *它的作用是将图片纹理沿XY轴进行复制
 *
 * X —-> CLAMP Y —-> MIRROR
 *
 * X —-> MIRROR Y —-> CLAMP
 *
 * X —-> CLAMP Y —-> REPEAT
 *
 * X —-> REPEAT Y —-> CLAMP
 *
 * X —-> REPEAT Y —-> MIRROR
 *
 * X —-> MIRROR Y —-> REPEAT
 */
public class CustomView extends View {

    private Bitmap mBitmap;
    private BitmapShader mShader;
    private LinearGradient mLinearGradient;
    private RadialGradient mRadialGradient;
    private SweepGradient mSweepGradient;

    private Paint mPaint;
    private Bitmap mMSrcBmp;

    public CustomView(Context context) {
        super(context);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mBitmap =
                BitmapFactory.decodeResource(getResources(),
                R.drawable.dog);
        mMSrcBmp = Bitmap.createScaledBitmap(mBitmap,400,400,false);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.CLAMP,BitmapShader.TileMode.CLAMP);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.MIRROR,BitmapShader.TileMode.MIRROR);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.REPEAT,BitmapShader.TileMode.REPEAT);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.CLAMP,BitmapShader.TileMode.MIRROR);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.CLAMP,BitmapShader.TileMode.REPEAT);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.REPEAT,BitmapShader.TileMode.CLAMP);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.REPEAT,BitmapShader.TileMode.MIRROR);

//        mShader = new BitmapShader(mBitmap,
//                BitmapShader.TileMode.MIRROR,BitmapShader.TileMode.REPEAT);

        mShader = new BitmapShader(mMSrcBmp,
                BitmapShader.TileMode.REPEAT,BitmapShader.TileMode.REPEAT);

        /*mLinearGradient=new LinearGradient(0,0,400,0,Color.parseColor("#faf84d"),
                Color.parseColor("#CC423C"), Shader.TileMode.CLAMP);*/

       /*
       float[] positon置为null，而它代表了什么呢？它其实与colors数组对应，
       代表了各个颜色值在位置，positions数组中的值大小范围从0.0到1.0，0.0代表起点位置，1.0代表终点位置。
       如果这个数组被置为空的话，颜色就会平均分配。
        */
//        mLinearGradient = new LinearGradient(0,0,400,0,new int[]{Color.parseColor("#faf84d"),Color.parseColor("#003449"),
//                Color.parseColor("#808080"),
//                Color.parseColor("#CC423C")},null,Shader.TileMode.CLAMP);

       /* mLinearGradient = new LinearGradient(0,0,400,0,new int[]{Color.parseColor("#faf84d"),Color.parseColor("#003449"),
                Color.parseColor("#808080"),
                Color.parseColor("#CC423C")},new float[]{0.6f,0.8f,0.2f,1.0f},Shader.TileMode.CLAMP);*/

        /*mRadialGradient = new RadialGradient(400/2,400/2,400/2,Color.parseColor("#faf84d"),
                Color.parseColor("#CC423C"), Shader.TileMode.CLAMP);*/

       /* mRadialGradient = new RadialGradient(400/2,400/2,400/2,new int[]{Color.parseColor("#00aa00"),Color.parseColor("#880033"),
                Color.parseColor("#F8795A"),
                Color.parseColor("#CC423C")},new float[]{0.0f,0.2f,0.8f,1.0f}, Shader.TileMode.CLAMP);*/

        /*mSweepGradient=new SweepGradient(400/2,400/2,Color.RED,Color.BLUE);*/
        mSweepGradient=new SweepGradient(400/2,400/2,new int[]{Color.RED,Color.CYAN,Color.YELLOW,
                Color.GREEN,Color.MAGENTA,Color.BLUE},new float[]{0.0f,0.2f,0.3f,0.4f,0.8f,1.0f});

        mPaint=new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(400,400);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

       /* int w=getWidth();
        int h=getHeight();

        int radius=w<=h?w/2:h/2;

        mPaint.setTextSize(200);
        mPaint.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        mPaint.setShader(mShader);

        canvas.drawText("狗狗",0,h/2,mPaint);*/

//        canvas.drawCircle(w/2,h/2,radius,mPaint);


//2  将Shader赋值给Paint对象。
        mPaint.setShader(mSweepGradient);

//3  绘制图形
        canvas.drawRect(0,0,getWidth(),getHeight(),mPaint);
    }
}
