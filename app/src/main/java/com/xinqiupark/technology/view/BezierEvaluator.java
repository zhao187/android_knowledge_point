package com.xinqiupark.technology.view;

import android.animation.TypeEvaluator;
import android.graphics.PointF;

import com.xinqiupark.technology.util.BezierUtil;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/18
 * desc:
 */
public class BezierEvaluator implements TypeEvaluator<PointF> {

    private PointF mControlPoint;

    public BezierEvaluator(PointF controlPoint) {
        this.mControlPoint = controlPoint;
    }


    @Override
    public PointF evaluate(float fraction, PointF startValue, PointF endValue) {
        return BezierUtil.CalculateBezierPointForQuadratic(fraction,startValue,mControlPoint,endValue);
    }
}
