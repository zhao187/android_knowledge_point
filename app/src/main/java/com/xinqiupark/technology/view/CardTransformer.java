package com.xinqiupark.technology.view;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/3
 * desc:卡片效果Transformer
 */
public class CardTransformer implements PageTransformer {
    private int mOffset = 60;

    @Override
    public void transformPage(View page, float position) {

        if (position <= 0) {
            page.setRotation(45 * position);
            page.setTranslationX((page.getWidth() / 2 * position));
        } else {
            //移动X轴坐标，使得卡片在同一坐标
            page.setTranslationX(-position * page.getWidth());
            //缩放卡片并调整位置
            float scale = (page.getWidth() - mOffset * position) / page.getWidth();
            page.setScaleX(scale);
            page.setScaleY(scale);
            //移动Y轴坐标
            page.setTranslationY(position * mOffset);
        }
    }
}
