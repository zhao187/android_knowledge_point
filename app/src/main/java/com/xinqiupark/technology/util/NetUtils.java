package com.xinqiupark.technology.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/7/20
 * desc:网络监听工具类
 */
public class NetUtils {

    /*
    判断当前网络是否连接
     */
    public static boolean isConnected(Context context)
    {
        ConnectivityManager connectivityManager= (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(null !=connectivityManager)
        {
            NetworkInfo info=connectivityManager.getActiveNetworkInfo();
            if(null!=info && info.getState()==NetworkInfo.State.CONNECTED)
            {
                return true;
            }
        }
        return false;
    }
}
