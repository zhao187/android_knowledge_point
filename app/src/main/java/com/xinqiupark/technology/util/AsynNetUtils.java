package com.xinqiupark.technology.util;

import android.os.Handler;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/8/28
 * desc:
 *
 * 自己封装的缺点:1.每次都new Thread，new Handler消耗过大
2.没有异常处理机制
3.没有缓存机制
4.没有完善的API(请求头,参数,编码,拦截器等)与调试模式
5.没有Https
 */
public class AsynNetUtils {
    public interface Callback {
        void onResponse(String response);
    }

    public static void get(final String url, final Callback callback) {
        final Handler handler = new Handler();
        new Thread(() -> {
            final String response = com.xinqiupark.technology.util.HttpNetUtils.get(url);
            handler.post(() -> callback.onResponse(response));
        }).start();
    }

    public static void post(final String url, final String content, final Callback callback) {
        final Handler handler = new Handler();
        new Thread(() -> {
            final String response = com.xinqiupark.technology.util.HttpNetUtils.post(url, content);
            handler.post(() -> callback.onResponse(response));
        }).start();
    }
}
