package com.xinqiupark.technology.util;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;


/**
 * @author Steven.zhao
 *         email:hongtu.zhao@goodwinsoft.net
 *         date:2018/4/9
 *         desc:界面常用工具类
 */

public class DensityUtil {
    /**
     * 1dp---1px;
     * 1dp---0.75px;
     * 1dp---0.5px;
     * ....
     *
     * @param dp
     * @return
     */
    public static int dip2px(Context context,int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density + 0.5);
    }

    ;

    public static int px2dip(Context context,int px) {
        float density =context.getResources().getDisplayMetrics().density;
        return (int) (px / density + 0.5);
    }
}
