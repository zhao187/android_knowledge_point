package com.xinqiupark.technology.bottom_navigation;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xinqiupark.technology.R;
import com.xinqiupark.technology.ui.fragment.CustomFragment;
import com.xinqiupark.technology.ui.fragment.HomeFragment;
import com.xinqiupark.technology.ui.fragment.MVPFragment;
import com.xinqiupark.technology.ui.fragment.NetWorkFragment;


/**
 * Created by zhouwei on 17/4/23.
 */

public class DataGenerator {

    public static final int []mTabRes = new int[]{R.drawable.tab_home_selector,R.drawable.tab_discovery_selector,R.drawable.tab_attention_selector,R.drawable.tab_profile_selector};
    public static final String[]mTabTitle = new String[]{"基础知识","自定义view","网络","mvp框架"};

    public static Fragment[] getFragments(){
        Fragment fragments[] = new Fragment[4];
        fragments[0] = HomeFragment.newInstance(mTabTitle[0]);
        fragments[1] = CustomFragment.newInstance(mTabTitle[1]);
        fragments[2] = NetWorkFragment.newInstance(mTabTitle[2]);
        fragments[3] = MVPFragment.newInstance(mTabTitle[3]);
        return fragments;
    }

    /**
     * 获取Tab 显示的内容
     * @param context
     * @param position
     * @return
     */
    public static View getTabView(Context context, int position){
        View view = LayoutInflater.from(context).inflate(R.layout.home_tab_content,null);
        ImageView tabIcon = (ImageView) view.findViewById(R.id.tab_content_image);
        tabIcon.setImageResource(DataGenerator.mTabRes[position]);
        TextView tabText = (TextView) view.findViewById(R.id.tab_content_text);
        tabText.setText(mTabTitle[position]);
        return view;
    }
}
