package com.xinqiupark.file;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/9/19
 * desc:
 */
public class LinkedHashTest {
    public static void main(String[] args)
    {
        LinkedHashMap<Integer,Integer> map=new LinkedHashMap<Integer, Integer>(0,0.75f,true);

        //插入数据
        map.put(0, 0);
        map.put(1, 1);
        map.put(2, 2);
        map.put(3, 3);
        map.put(4, 4);
        map.put(5, 5);
        map.put(6, 6);

        //访问数据
        map.get(1);
        map.get(2);

        //遍历集合
        for (Map.Entry<Integer,Integer> entry:map.entrySet())
        {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }
}
